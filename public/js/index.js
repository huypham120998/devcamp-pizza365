  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const baseUrl = "/devcamp-pizza365/orders";
  // bạn có thể dùng để lưu trữ combo được chọn, 
  // mỗi khi khách chọn menu S, M, L bạn lại đổi giá trị properties của nó
  var gSelectedMenuStructure = {
    menuName: "",    // S, M, L
    duongKinhCM: 0,
    suonNuong: 0,
    saladGr: 0,
    drink: 0,
    priceVND: 0
  }
  // bạn có thể dùng để lưu loại pizza đươc chọn, mỗi khi khách chọn, bạn lại đổi giá trị cho nó
  var gSelectedPizzaType = "";
  // danh sách các mã giảm giá
  var gDiscountVouchers = {

  };
  var gPersonOrder = {
    menuDuocChon: "",
    loaiPizza: "",
    hoVaTen: "",
    email: "",
    dienThoai: "",
    diaChi: "",
    loiNhan: "",
    voucher: "",
    drinkID: "",
    drink: "",
  };
  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onLoadPaging();
  $("#btn-small-size").click(function () {
    btnSmallSizeClick();
  })
  $("#btn-medium-size").click(function () {
    btnMediumSizeClick();
  })
  $("#btn-large-size").click(function () {
    btnLargeSizeClick();
  })
  $("#btn-seafood").click(function () {
    changeTypePizzaClick("seafood");
  })
  $("#btn-hawaii").click(function () {
    changeTypePizzaClick("hawaii");
  })
  $("#btn-bacon").click(function () {
    changeTypePizzaClick("bacon");
  })
  $("#btn-send-order").click(function () {
    btnSendOrderClick();
  })
  $("#btn-create-order").click(function () {
    btnCreateOrderClick();
  })
 
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  function onLoadPaging() {
    callApiAndLoadDrinkSelect();
  }
  function changeTypePizzaClick(paramPizza) {
    gSelectedPizzaType = paramPizza;
    console.log(gSelectedPizzaType);
    changeTypePizzaButtonColor(paramPizza);
  }
  function btnSmallSizeClick() {
    var vSelectedSizeStructure = getSize("S", 20, 2, 200, 2, 150000);
    // gọi method hiển thị thông tin
    vSelectedSizeStructure.displayInConsoleLog();
    // đổi màu button
    changePlanButtonColor("Small");
    gSelectedMenuStructure = vSelectedSizeStructure;
  }
  function btnMediumSizeClick() {
    var vSelectedSizeStructure = getSize("M", 25, 4, 300, 3, 200000);
    // gọi method hiển thị thông tin
    vSelectedSizeStructure.displayInConsoleLog();
    // đổi màu button
    changePlanButtonColor("Medium");
    gSelectedMenuStructure = vSelectedSizeStructure;
  }
  function btnLargeSizeClick() {
    //tạo một đối tượng, được tham số hóa
    var vSelectedSizeStructure = getSize("L", 30, 8, 500, 4, 250000);
    // gọi method hiển thị thông tin
    vSelectedSizeStructure.displayInConsoleLog();
    // đổi màu button
    changePlanButtonColor("Large");
    gSelectedMenuStructure = vSelectedSizeStructure;
  }
  function btnSendOrderClick() {
    console.log("lưu dữ liệu đơn hàng");
    // khai báo đối tượng môn học chứa dữ liệu
    // B1: thu thập dữ liệu
    getPersonOrder(gPersonOrder);
    // B2: kiểm tra dữ liệu
    var vDuLieuHopLe = validatePersonOrder(gPersonOrder);
    if (vDuLieuHopLe) {
      // B3: ghi dữ liệu vào vùng màu vàng
      console.log("show dữ liệu");
      debugger;
      showPersonOrder(gPersonOrder);
      $("#create-order-modal").modal("show");
    }
    else {
      console.log(vDuLieuHopLe);
    }
  }
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  function btnCreateOrderClick() {
      if(gPersonOrder.voucher != "" && gDiscountVouchers != ""){
        var vDiscount = gDiscountVouchers.phanTramGiamGia;
      }
      else{
        var vDiscount = 0;
      }
      var fee = ((100 - parseInt(vDiscount)) * parseInt(gSelectedMenuStructure.priceVND))/100;
      console.log("Họ và tên: " + gPersonOrder.hoVaTen);
      console.log("Email: " + gPersonOrder.email);
      console.log("Số điện thoại: " + gPersonOrder.dienThoai);
      console.log("Lời nhắn: " + gPersonOrder.loiNhan);
      console.log("Kích cỡ: " + gPersonOrder.menuDuocChon);
      console.log("Đường kính: " + gSelectedMenuStructure.duongKinhCM);
      console.log("Sườn nướng: " + gSelectedMenuStructure.suonNuong);
      console.log("Salad: " + gSelectedMenuStructure.saladGr);
      console.log("Nước ngọt: " + gSelectedMenuStructure.drink);
      console.log("Loại Pizza: " + gPersonOrder.loaiPizza);
      console.log("Loại nước uống: " + gPersonOrder.drink);
      console.log("Mã voucher: " + gPersonOrder.voucher);
      console.log("Giá: " + gSelectedMenuStructure.priceVND + "VND");
      console.log("Salad: " + gSelectedMenuStructure.saladGr);
      console.log("Nước ngọt: " + gSelectedMenuStructure.drink);
      console.log("Discount % : " + vDiscount);
      console.log("Phí thanh toán VND: " + fee);
      var vObjectOrder = {
        kichCo: gPersonOrder.menuDuocChon,
        duongKinh: gSelectedMenuStructure.duongKinhCM,
        suon:  gSelectedMenuStructure.suonNuong,
        salad: gSelectedMenuStructure.saladGr,
        loaiPizza: gPersonOrder.loaiPizza,
        idVourcher: gPersonOrder.voucher,
        idLoaiNuocUong: gPersonOrder.drinkID,
        soLuongNuoc: gSelectedMenuStructure.drink,
        hoTen: gPersonOrder.hoVaTen,
        thanhTien: fee,
        email: gPersonOrder.email,
        soDienThoai: gPersonOrder.dienThoai,
        diaChi: gPersonOrder.diaChi,
        loiNhan: gPersonOrder.loiNhan
      }
      callApiPostOrder(vObjectOrder);
    }
    function callApiPostOrder(paramOrder){
      $.ajax({
      url: "/devcamp-pizza365/orders",
      type: "POST",
      data: JSON.stringify(paramOrder),
      contentType: "application/json;charset=UTF-8",
      dataType: 'json',
      async: false,
      success: function (responseObject) {
        //debugger;
        debugger;
        $("#input-order-code").val(responseObject.data.orderCode);
        $("#create-order-modal").modal("hide");
        $("#notify-order-modal").modal("show");
      },
      error: function (error) {
        console.assert(error.responseText);
      }
    });
    }
  function showPersonOrder(paramPersonOrderObj) {
      var vName = $("#input-create-name");
      var vPhone = $("#input-create-phone");
      var vAddress = $("#input-create-address");
      var vMessage = $("#input-create-message");
      var vVoucher = $("#input-create-voucher");
      var vInfoDetail = $("#text-area-info");
      vName.val(paramPersonOrderObj.hoVaTen);
      vPhone.val(paramPersonOrderObj.dienThoai);
      vAddress.val(paramPersonOrderObj.diaChi);
      vMessage.val(paramPersonOrderObj.loiNhan);
      vVoucher.val(paramPersonOrderObj.voucher);
      debugger;
      callApiGetDiscountVoucher(paramPersonOrderObj.voucher);
      debugger;
      if(gPersonOrder.voucher != "" && gDiscountVouchers.data != null){
        var vDiscount = gDiscountVouchers.data.phanTramGiamGia;
      }
      else{
        var vDiscount = 0;
      }
      var fee = ((100 - parseInt(vDiscount)) * parseInt(gSelectedMenuStructure.priceVND))/100;
      var vInfoValue = "Xác nhận: " + paramPersonOrderObj.hoVaTen + ", " + paramPersonOrderObj.dienThoai + ", " + paramPersonOrderObj.diaChi + "\n";
      vInfoValue +=  "Menu: " + gSelectedMenuStructure.menuName + ", sườn nướng " + gSelectedMenuStructure.suonNuong + ", nước " + gSelectedMenuStructure.drink + "\n";
      vInfoValue +=  "loại Pizza: " + paramPersonOrderObj.loaiPizza + ", Giá: " + gSelectedMenuStructure.priceVND + " vnd"+ ", Mã giảm giá: " + paramPersonOrderObj.voucher + "\n";
      vInfoValue +=  "Phải thanh toán: " + fee + " vnd (giảm giá " + vDiscount + "%)";
      vInfoDetail.html(vInfoValue);
      // if(paramPersonOrderObj.voucher != "" && gDiscountVouchers != ""){
      //   var vDiscount = gDiscountVouchers.phanTramGiamGia;
      //   console.log(gDiscountVouchers);
      //   debugger;
      // }
      // else{
      //   var vDiscount = 0;
      //}
      // var fee = (100 - parseInt(vDiscount)) * parseInt(gSelectedMenuStructure.priceVND);
      // hiển thị thông tin đơn đặt hàng
      // vDivContainerOrder.style.display = "block";
      // vDivOrderInfor.innerHTML = "Họ và tên: " + paramPersonOrderObj.hoVaTen + "<br>";
      // vDivOrderInfor.innerHTML += "Email: " + paramPersonOrderObj.email + "<br>";
      // vDivOrderInfor.innerHTML += "Số điện thoại: " + paramPersonOrderObj.dienThoai + "<br>";
      // vDivOrderInfor.innerHTML += "Lời nhắn: " + paramPersonOrderObj.loiNhan + "<br>";
      // vDivOrderInfor.innerHTML += "......................" + "<br>";
      // vDivOrderInfor.innerHTML += "Kích cỡ: " + paramPersonOrderObj.menuDuocChon + "<br>";
      // vDivOrderInfor.innerHTML += "Đường kính: " + gSelectedMenuStructure.duongKinhCM + "<br>";
      // vDivOrderInfor.innerHTML += "Sườn nướng: " + gSelectedMenuStructure.suonNuong + "<br>";
      // vDivOrderInfor.innerHTML += "Salad: " + gSelectedMenuStructure.saladGr + "<br>";
      // vDivOrderInfor.innerHTML += "Nước ngọt: " + gSelectedMenuStructure.drink + "<br>";
      // vDivOrderInfor.innerHTML += "......................" + "<br>";
      // vDivOrderInfor.innerHTML += "Loại Pizza: " + paramPersonOrderObj.loaiPizza + "<br>";
      // vDivOrderInfor.innerHTML += "Loại nước uống: " + paramPersonOrderObj.drink + "<br>";
      // vDivOrderInfor.innerHTML += "Mã voucher: " + paramPersonOrderObj.voucher + "<br>";
      // vDivOrderInfor.innerHTML += "Giá: " + gSelectedMenuStructure.priceVND + "VND" + "<br>";
      // vDivOrderInfor.innerHTML += "Salad: " + gSelectedMenuStructure.saladGr + "<br>";
      // vDivOrderInfor.innerHTML += "Nước ngọt: " + gSelectedMenuStructure.drink + "<br>";
      // vDivOrderInfor.innerHTML += "Discount % : " + vDiscount + "<br>";
      // vDivOrderInfor.innerHTML += "Phí thanh toán VND: " + fee + "<br>";
      console.log(paramPersonOrderObj);
    }
    function callApiGetDiscountVoucher(paramVoucher){
        debugger;
      $.ajax({
        url: "/voucherCode/?maVoucher="+ paramVoucher,
      type: "GET",
      dataType: 'json',
      async: false,
      success: function (responseObject) {
        //debugger;
        gDiscountVouchers = responseObject;
        debugger;
      },
      error: function (error) {
        console.assert(error.responseText);
        gDiscountVouchers = "";
      }
    });
    }
  function getPersonOrder(paramPersonOrderObj) {
    var vHoTen = $("#inp-fullname");
    var vEmail = $("#inp-email");
    var vDiaChi = $("#inp-dia-chi");
    var vSoDienThoai = $("#inp-dien-thoai");
    var vCoucher = $("#inp-voucher");
    var vSelectDrink = $("#select-drink");
    var vMessage = $("#inp-message");
    paramPersonOrderObj.menuDuocChon = gSelectedMenuStructure.menuName;
    paramPersonOrderObj.loaiPizza = gSelectedPizzaType;
    paramPersonOrderObj.hoVaTen = $.trim(vHoTen.val());
    paramPersonOrderObj.diaChi = $.trim(vDiaChi.val());
    paramPersonOrderObj.voucher = $.trim(vCoucher.val());
    debugger;
    paramPersonOrderObj.drinkID = $.trim(vSelectDrink.val());
    paramPersonOrderObj.drink = $("#select-drink :selected").text();
    paramPersonOrderObj.loiNhan = $.trim(vMessage.val());
    paramPersonOrderObj.dienThoai = $.trim(vSoDienThoai.val());
    paramPersonOrderObj.email = $.trim(vEmail.val());
  }
  function validatePersonOrder(paramPersonOrderObj) {
    if (paramPersonOrderObj.hoVaTen == "") {
      alert("Họ và tên chưa được nhập vào");
      return false;
    }
    if (paramPersonOrderObj.email == "") {
      alert("Email chưa được nhập vào");
      return false;
    }
    if (!ValidateEmail(paramPersonOrderObj.email)) {
      alert("Email chưa hợp lệ");
      return false;
    }
    if (paramPersonOrderObj.diaChi == "") {
      alert("Địa chỉ chưa được nhập vào");
      return false;
    }
    if (paramPersonOrderObj.dienThoai == "") {
      alert("Số điện thoại chưa được nhập vào");
      return false;
    }
    if (paramPersonOrderObj.menuDuocChon == "") {
      alert("Bạn chưa chọn kích cỡ");
      return false;
    }
    if (paramPersonOrderObj.loaiPizza == "") {
      alert("Bạn chưa chọn loại pizza");
      return false;
    }
    if (paramPersonOrderObj.drinkID == "0") {
      alert("Bạn chưa chọn loại đồ uống");
      return false;
    }
    if (paramPersonOrderObj.voucher == "") {
      alert("Bạn nhập mã giảm giá");
      return false;
    }
    return true;
  }
  function ValidateEmail(email) {
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (email.match(mailformat)) {
      return true;
    }
    else {
      return false;
    }
  }

  function callApiAndLoadDrinkSelect() {
    $.ajax({
      url: "/devcamp-pizza365/drinks",
      type: "GET",
      dataType: 'json',
      async: false,
      success: function (responseObject) {
        //debugger;
        loadDataToDrinkSelect(responseObject);
      },
      error: function (error) {
        console.assert(error.responseText);
      }
    });
  }

  function loadDataToDrinkSelect(paramSelect) {
    console.log(paramSelect);
    var vSelectDrink = $("#select-drink");
    for (var bI = 0; bI < paramSelect.drinks.length; bI++) {

      var option = $('<option/>');
      option.attr({ 'value': paramSelect.drinks[bI].maNuocUong }).text(paramSelect.drinks[bI].tenNuocUong);
      vSelectDrink.append(option);
    }
  }

  //đổi màu button để phân biện được chọn
  function changePlanButtonColor(paramSize) {
    var vBtnSmall = $("#btn-small-size");
    var vBtnMedium = $("#btn-medium-size");
    var vBtnLarge = $("#btn-large-size");
    var vTitleSmall = $("#small-size");
    var vTitleMedium = $("#medium-size");
    var vTitleLarge = $("#large-size");

    if (paramSize === "Small") {
      vBtnSmall.attr("class", "btn bg-select w-100");
      vBtnMedium.attr("class", "btn w-100 bg-orange");
      vBtnLarge.attr("class", "btn w-100 bg-orange");
      vTitleSmall.attr("class", "card-header bg-select text-dark text-center");
      vTitleMedium.attr("class", "card-header bg-orange text-dark text-center");
      vTitleLarge.attr("class", "card-header bg-orange text-dark text-center");
    }
    else if (paramSize === "Medium") {
      vBtnSmall.attr("class", "btn w-100 bg-orange");
      vBtnMedium.attr("class", "btn bg-select w-100");
      vBtnLarge.attr("class", "btn w-100 bg-orange");
      vTitleSmall.attr("class", "card-header bg-orange text-dark text-center");
      vTitleMedium.attr("class", "card-header bg-select text-dark text-center");
      vTitleLarge.attr("class", "card-header bg-orange text-dark text-center");
    }
    else if (paramSize === "Large") {
      vBtnSmall.attr("class", "btn bg-select w-100");
      vBtnMedium.attr("class", "btn bg-select w-100");
      vBtnLarge.attr("class", "btn bg-select w-100");
      vTitleSmall.attr("class", "card-header bg-orange text-dark text-center");
      vTitleMedium.attr("class", "card-header bg-orange text-dark text-center");
      vTitleLarge.attr("class", "card-header bg-select text-dark text-center");
    }
  }
  function changeTypePizzaButtonColor(paramType) {
    var vBtnSeafood = $("#btn-seafood");
    var vBtnHawaii = $("#btn-hawaii");
    var vBtnBacon = $("#btn-bacon");

    if (paramType === "seafood") {
      vBtnSeafood.attr("class", "btn bg-select w-100");
      vBtnHawaii.attr("class", "btn bg-orange w-100");
      vBtnBacon.attr("class", "btn bg-orange w-100");
    }
    if (paramType === "hawaii") {
      vBtnSeafood.attr("class", "btn bg-orange w-100");
      vBtnHawaii.attr("class", "btn bg-select w-100");
      vBtnBacon.attr("class", "btn bg-orange w-100");
    }
    if (paramType === "bacon") {
      vBtnSeafood.attr("class", "btn bg-orange w-100");
      vBtnHawaii.attr("class", "btn bg-orange w-100");
      vBtnBacon.attr("class", "btn bg-select w-100");
    }
  }
  //function trả lại một đối tượng plan được tham số 
  function getSize(parammenuName, paramDuongKinhCM, paramSuonNuong, paramsaladGr, paramDrink, paramPriceVND) {
    // Define plan structure object
    var vSelectedSizeStructure = {
      menuName: parammenuName,
      duongKinhCM: paramDuongKinhCM,
      suonNuong: paramSuonNuong,
      saladGr: paramsaladGr,
      drink: paramDrink,
      priceVND: paramPriceVND,
      // method display size infor
      displayInConsoleLog() {
        console.log("Size " + this.menuName);
        console.log("Đường kính: " + this.duongKinhCM + "cm");
        console.log("Sườn nướng: " + this.suonNuong);
        console.log("Salad: " + this.saladGr + "gram");
        console.log("Nước ngọt: " + this.drink);
        console.log("Giá: " + this.priceVND + " VND");
      }
    }
    return vSelectedSizeStructure;  //trả lại đối tượng, có đủ dữ liệu (attribute) và các methods (phương thức)
  }
