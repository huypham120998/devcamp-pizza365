// Import thư viện express.js vào. Dạng Import express from "express";
const express = require("express");
// Import thư viện mongoosejs
const mongoose = require("mongoose");

// Import router Module
const drinkRouter = require("./app/routes/drinkRouter");
const voucherRouter = require("./app/routes/voucherRouter");
const userRouter = require("./app/routes/userRouter");
const orderRouter = require("./app/routes/orderRouter");
const order2Router = require("./app/routes/order2Router");

// import thư viện path
const path = require("path");

// Khởi tạo app express
const app = express();


// Khai báo sẵn 1 port trên hệ thống
const port = 8000;


app.use(express.json());


// khai báo middleware đọc dữ liệu utf-8
app.use(express.urlencoded({
    extended: true
}))
app.use(express.static(__dirname + `/public`));

app.use((req, res, next) => {
    let today = new Date();
    console.log("Current: ", today);

    next();
})

app.use((req, res, next) => {
    console.log("Method: ", req.method);

    next();
})

mongoose.connect("mongodb://localhost:27017/CRUD_Pizza365", (error) => {
    if (error) throw error;

    console.log("Connnect successfully!")
});

// Khai báo API
app.get("/", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/index.html"))
})


app.use(drinkRouter);
app.use(voucherRouter);
app.use(userRouter);
app.use(orderRouter);
app.use(order2Router);
// Callback function
// + Nó là 1 function
// + Nó là 1 tham số của 1 hàm khác
// + Nó sẽ chạy sau khi hàm chủ thể được thực hiện
// Lắng nghe app trên cổng được khai báo 8000
app.listen(port, () => {
    console.log("App listening on port: ", port);
});
