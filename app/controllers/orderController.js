// Import thư viện mongoose
const mongoose = require("mongoose");

// Import Order Model
const orderModel = require("../models/orderModel");
const userModel = require("../models/userModel");


// Create Order
const createOrder = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    let userId = req.params.userId;
    let body = req.body;
    console.log(userId);
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "User ID is invalid"
        })
    }

    if(!body.orderCode) {
        return res.status(400).json({
            message: "orderCode is invalid"
        })
    }
    
    if(!body.pizzaSize) {
        return res.status(400).json({
            message: "Pizza Size is required"
        })
    }
    
    if(!body.pizzaType) {
        return res.status(400).json({
            message: "Pizza Type is required"
        })
    }

    if (!body.drink) {
        return res.status(400).json({
             message: 'Drink is required!'
        })
   }
  

    //B3: Thao tác với cơ sở dữ liệu
    let newOrderInput = {
        _id: mongoose.Types.ObjectId(),
        orderCode: body.orderCode,
        pizzaSize: body.pizzaSize,
        pizzaType: body.pizzaType,
        voucher: body.voucher,
        drink: body.drink,
        status: body.status,
    }

    orderModel.create(newOrderInput, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            userModel.findByIdAndUpdate(userId, 
                {
                   $push: { orders: data._id } 
                },
                (err, updatedUser) => {
                    if(err) {
                        return ResizeObserver.status(500).json({
                            status: "Error 500: Internal server error",
                            message: err.message
                        })
                    } else {
                        return res.status(201).json({
                            status: "Create Order Success",
                            data: data
                        })
                    }
                }
            )
        }
    })
}
// Get all Order 
const getAllOrder = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    orderModel.find((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Get all order success",
                data: data
            })
        }
    })
}

const getOrderById = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    let orderId = req.params.orderId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "OrderId ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    orderModel.findById(orderId, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Get order success",
                data: data
            })
        }
    })
}

const updateOrderById = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    let orderId = req.params.orderId;
    let body = req.body;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "Order ID is not valid"
        })
    }
    if(body.orderCode !== undefined && body.orderCode == "") {
        return res.status(400).json({
            message: "Order Code is required!"
        })
    }
    if(body.pizzaSize !== undefined && body.pizzaSize == "") {
        return res.status(400).json({
            message: "Order Code is required!"
        })
    }
    if(body.pizzaType !== undefined && body.pizzaType == "") {
        return res.status(400).json({
            message: "Order Code is required!"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(body.voucher)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "Voucher ID is invalid"
        })
    } 
     if(!mongoose.Types.ObjectId.isValid(body.drink)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "drink ID is invalid"
        })
    }
    if(body.status !== undefined && body.status == "") {
        return res.status(400).json({
            message: "Status Code is required!"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    let OrderUpdate = {
        orderCode: body.orderCode,
        pizzaSize: body.pizzaSize,
        pizzaType: body.pizzaType,
        voucher: body.voucher,
        drink: body.drink,
        status: body.status,
    }


    orderModel.findByIdAndUpdate(orderId, OrderUpdate, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Update order success",
                data: data
            })
        }
    })
}

const deleteOrderById = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    let userId = req.params.userId;
    let orderId = req.params.orderId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "User ID is not valid"
        })
    }

    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "Order ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    orderModel.findByIdAndDelete(orderId, (error) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            // Sau khi xóa xong 1 order khỏi collection cần xóa thêm orderID trong course đang chứa nó
            userModel.findByIdAndUpdate(userId, 
                {
                    $pull: { orders: orderId } 
                },
                (err, updatedUser) => {
                    if(err) {
                        return res.status(500).json({
                            status: "Error 500: Internal server error",
                            message: err.message
                        })
                    } else {
                        return res.status(204).json({
                            status: "Success: Delete order success"
                        })
                    }
                })
        }
    })
}
  // Export User controller thành 1 module
module.exports = {
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrderById,
    deleteOrderById
}