// Import thư viện mongoose
const mongoose = require("mongoose");

// Import Voucher Model
const voucherModel = require("../models/VoucherModel");

// Create Voucher
const createVoucher = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let body = req.body;

    // B2: Validate dữ liệu
    if(!body.maVoucher) {
        return res.status(400).json({
            message: "ma nuoc uong is la bat buoc!"
        })
    }
    if(!Number.isInteger(body.phanTramGiamGia) || body.phanTramGiamGia < 0) {
        return res.status(400).json({
            message: "Phan Tram Giam Gia is invalid!"
        })
    }

    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newVoucherData = {
        _id: mongoose.Types.ObjectId(),
        maVoucher: body.maVoucher,
        phanTramGiamGia: body.phanTramGiamGia,
        ghiChu: body.ghiChu
    }

    voucherModel.create(newVoucherData, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(201).json({
            message: "Create successfully",
            newVoucher: data
        })
    })
}

// Get all Voucher 
const getAllVoucher = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    // B2: Validate dữ liệu
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    voucherModel.find((error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(200).json({
            message: "Get all vouchers successfully",
            vouchers: data
        })
    })
}

// Get Voucher by id
const getVoucherById = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let voucherId = req.params.voucherId;
    console.log("voucher", voucherId);
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: "voucher ID is invalid!"
        })
    }

    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    voucherModel.findById(voucherId, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(201).json({
            message: "Get voucher successfully",
            voucher: data
        })
    })
}
// lấy voucher bằng code
const getVoucherByCode = (request, response) => {
    //b1: thu thập dữ liệu
    let maVoucher = request.query.maVoucher;
    console.log(maVoucher);
    // validate
    if (!maVoucher) {
        response.status(400).json({
            status: "Error 400: bad request",
            message: "Voucher ID is not valid"
        })
    }
    // sử dụng dữ liệu
    voucherModel.findOne({ maVoucher: request.query.maVoucher }, (error, data) => {
        console.log(data);
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        }
        else {
            response.status(200).json({
                status: "Success: Get voucher by code success",
                data: data
            })
        }
    })
}

// Update Voucher by id
const updateVoucherById = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let voucherId = req.params.voucherId;
    let body = req.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: "voucher ID is invalid!"
        })
    }

    // Bóc tách trường hợp undefied
    if(body.maVoucher !== undefined && body.maVoucher == "") {
        return res.status(400).json({
            message: "Ma Voucher la bat buoc!"
        })
    }


    if(body.phanTramGiamGia !== undefined && (!Number.isInteger(body.phanTramGiamGia) || body.phanTramGiamGia < 0)) {
        return res.status(400).json({
            message: "Phan Tram Giam Gia is invalid!"
        })
    }

    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    let voucherUpdate = {
        maVoucher: body.maVoucher,
        phanTramGiamGia: body.phanTramGiamGia,
        ghiChu: body.ghiChu
    };

    voucherModel.findByIdAndUpdate(voucherId, voucherUpdate, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(200).json({
            message: "Update Voucher successfully",
            updatedVoucher: data
        })
    })
}

// Delete Voucher by id
const deleteVoucherById = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let voucherId = req.params.voucherId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: "voucher ID is invalid!"
        })
    }

    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    voucherModel.findByIdAndDelete(voucherId, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(204).json({
            message: "Delete voucher successfully"
        })
    })
}

// Export Voucher controller thành 1 module
module.exports = {
    createVoucher,
    getAllVoucher,
    getVoucherByCode,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}


