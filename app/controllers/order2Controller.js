const mongoose = require("mongoose");

const userModel = require('../models/userModel');
const orderModel = require('../models/orderModel')
const drinkModel = require('../models/drinkModel');

const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
function validateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        return true;
    }
    return false;
}
//khai báo random orderCode
function generateString(length) {
    let result = '';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

// Create order
const createOrder = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let fullName = req.body.hoTen;
    let email = req.body.email;
    let address = req.body.diaChi;
    let phone = req.body.soDienThoai;

    let orderCode = generateString(10);
    let pizzaSize = req.body.kichCo;
    let pizzaType = req.body.loaiPizza;
    let voucher = req.body.idVourcher;
    let drink = req.body.idLoaiNuocUong;
    // B2: Validate dữ liệu
    if (!fullName) {
         return res.status(400).json({
              message: 'Full Name is required!'
         })
    }

    if (!validateEmail(email)) {
         return res.status(400).json({
              message: 'Email is invalid!'
         })
    }
    if (!address) {
         return res.status(400).json({
              message: 'Address is required!'
         })
    }
    if (!phone) {
         return res.status(400).json({
              message: 'Phone is required!'
         })
    }
    if (!pizzaSize) {
         return res.status(400).json({
              message: 'Pizza Size is required!'
         })
    }
    if (!pizzaType) {
         return res.status(400).json({
              message: 'Pizza Type is required!'
         })
    }
    if (!drink) {
         return res.status(400).json({
              message: 'Drink is required!'
         })
    }

    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    userModel.findOne({ email: email }, (error, userExisted) => {
         if (error) {
              return res.status(500).json({
                   message: error.message
              })
         } else {
              // Nếu user ko tồn tại, tạo mới user
              if (!userExisted) {
                   let newUser = {
                        _id: mongoose.Types.ObjectId(),
                        fullName: fullName,
                        email: email,
                        address: address,
                        phone: phone
                   }
                   userModel.create(newUser, (error, newUserCreated) => {
                        if (error) {
                             return res.status(500).json({
                                  message: error.message
                             })
                        } else {
                             // Tạo order mới
                             let newOrder = {
                                  _id: mongoose.Types.ObjectId(),
                                  orderCode: orderCode,
                                  pizzaSize: pizzaSize,
                                  pizzaType: pizzaType,
                                  voucher: voucher,
                                  drink: drink,
                             }
                             orderModel.create(newOrder, (error, newOrderCreated) => {
                                  if (error) {
                                       return res.status(500).json({
                                            message: error.message
                                       })
                                  } else {
                                       userModel.findByIdAndUpdate(newUserCreated._id,
                                            {
                                                 $push: { orders: newOrderCreated._id }
                                            })
                                            .populate('orders')
                                            .exec((error, data) => {
                                                 if (error) {
                                                      return res.status(500).json({
                                                           message: error.message
                                                      })
                                                 } else {
                                                      return res.status(201).json({
                                                           message: 'Create a new user and order successfully',
                                                           data: newOrderCreated
                                                      })
                                                 }
                                            })
                                  }
                             })
                        }
                   })
              } else {
                   // Nếu user đã tồn tại, tạo order mới luôn
                   let newOrder = {
                        _id: mongoose.Types.ObjectId(),
                        orderCode: orderCode,
                        pizzaSize: pizzaSize,
                        pizzaType: pizzaType,
                        voucher: voucher,
                        drink: drink,
                   }
                   orderModel.create(newOrder, (error, newOrderCreated) => {
                        if (error) {
                             return res.status(500).json({
                                  message: error.message
                             })
                        } else {
                             userModel.findByIdAndUpdate(userExisted._id,
                                  {
                                       $push: { orders: newOrderCreated._id }
                                  })
                                  .populate('orders')
                                  .exec((error, data) => {
                                       if (error) {
                                            return res.status(500).json({
                                                 message: error.message
                                            })
                                       } else {
                                            return res.status(201).json({
                                                 message: 'Create a new order successfully',
                                                 data: newOrderCreated
                                            })
                                       }
                                  })
                        }
                   })
              }
         }
    })
}


// get drink ref
const getDrinkByModel = (request, response) => {
    drinkModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                message: error.message
            })
        }

        return response.status(200).json({
            message: "Get all drinks successfully",
            drinks: data
        })
    })
}

module.exports = {
    createOrder,
    getDrinkByModel

}
