// Import thư viện mongoose
const mongoose = require("mongoose");

// Import User Model
const userModel = require("../models/userModel");
function validateEmail(mail) 
{
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
  {
    return true;
  }
    return false;
}
// Create User
const createUser = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let body = req.body;

  // B2: Validate dữ liệu
  if(!body.fullName) {
      return res.status(400).json({
          message: "fullname is required!"
      })
  }
  if(!validateEmail(body.email)) {
      return res.status(400).json({
        message: "email is invalid!"
      })
  }
  if(!body.address) {
      return res.status(400).json({
        message: "address is required!"
      })
  }
  if(!body.phone) {
    return res.status(400).json({
      message: "phone is required!"
    })
}

  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  let newUserData = {
      _id: mongoose.Types.ObjectId(),
      fullName: body.fullName,
      email: body.email,
      address: body.address,
      phone: body.phone,
  }

  userModel.create(newUserData, (error, data) => {
      if(error) {
          return res.status(500).json({
              message: error.message
          })
      }

      return res.status(201).json({
          message: "Create successfully",
          newUser: data
      })
  })

}
// Get all User
const getAllUser= (req, res) => {
    // B1: Thu thập dữ liệu từ req
    // B2: Validate dữ liệu
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    userModel.find((error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(200).json({
            message: "Get all users successfully",
            users: data
        })
    });
}

const getAllLimitUser = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    // B2: Validate dữ liệu
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    var limit = req.query.limit;
    userModel.find().limit(limit).exec((error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(200).json({
            message: "Get limit users successfully",
            users: data
        })
    });
    // userModel.find((error, data) => {
    //     if(error) {
    //         return res.status(500).json({
    //             message: error.message
    //         })
    //     }

    //     return res.status(200).json({
    //         message: "Get all users successfully",
    //         users: data
    //     })
    // });
}

const getAllSkipUser = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    var skip = req.query.skip;
    // B2: Validate dữ liệu
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    userModel.find().skip(skip).exec((error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(200).json({
            message: "Get skip users successfully",
            users: data
        })
    });
    // userModel.find((error, data) => {
    //     if(error) {
    //         return res.status(500).json({
    //             message: error.message
    //         })
    //     }

    //     return res.status(200).json({
    //         message: "Get all users successfully",
    //         users: data
    //     })
    // });
}

const getAllSortUser = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    var sort = req.query.sort;
    // B2: Validate dữ liệu
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    userModel.find().sort({ fullName : sort}).exec((error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(200).json({
            message: "Get sort users successfully",
            users: data
        })
    });
    // userModel.find((error, data) => {
    //     if(error) {
    //         return res.status(500).json({
    //             message: error.message
    //         })
    //     }

    //     return res.status(200).json({
    //         message: "Get all users successfully",
    //         users: data
    //     })
    // });
}

const getAllSkipLimitUser = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    var skip = req.query.skip;
    var limit = req.query.limit;
    // B2: Validate dữ liệu
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    userModel.find().skip(skip).limit(limit).exec((error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(200).json({
            message: "Get sort users successfully",
            users: data
        })
    });
    // userModel.find((error, data) => {
    //     if(error) {
    //         return res.status(500).json({
    //             message: error.message
    //         })
    //     }

    //     return res.status(200).json({
    //         message: "Get all users successfully",
    //         users: data
    //     })
    // });
}

const getAllSortSkipLimitUser = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    var skip = req.query.skip;
    var limit = req.query.limit;
    var sort = req.query.sort;
    // B2: Validate dữ liệu
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    userModel.find().skip(skip).limit(limit).sort({fullName: sort}).exec((error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(200).json({
            message: "Get sort users successfully",
            users: data
        })
    });
    // userModel.find((error, data) => {
    //     if(error) {
    //         return res.status(500).json({
    //             message: error.message
    //         })
    //     }

    //     return res.status(200).json({
    //         message: "Get all users successfully",
    //         users: data
    //     })
    // });
}
// Get User by id
const getUserById = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let userId = req.params.userId;
    console.log("drink", userId);
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "User ID is invalid!"
        })
    }

    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    userModel.findById(userId, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(201).json({
            message: "Get User successfully",
            user: data
        })
    })
}

// Update Drink by id
const updateUserById  = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let userId = req.params.userId;
    let body = req.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "Users ID is invalid!"
        })
    }

    // Bóc tách trường hợp undefied
    if(body.fullName !== undefined && body.fullName == "") {
        return res.status(400).json({
            message: "fullname is required!"
        })
    }

    if(!validateEmail(body.email)) {
        return res.status(400).json({
          message: "email is invalid!"
        })
    }

    if(body.address !== undefined && body.address == "") {
        return res.status(400).json({
            message: "adress is required!"
        })
    }

    if(body.phone !== undefined && body.phone == "") {
        return res.status(400).json({
            message: "phone is required!"
        })
    }



    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    let userUpdate = {
        fullName: body.fullName,
        email: body.email,
        donGia: body.donGia,
        address: body.address,
        phone: body.phone
    };

    userModel.findByIdAndUpdate(userId, userUpdate, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(200).json({
            message: "Update user successfully",
            updatedDrink: data
        })
    })
}


// User Drink by id
const deleteUserById  = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let userId = req.params.userId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "User ID is invalid!"
        })
    }

    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    userModel.findByIdAndDelete(userId, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }

        return res.status(204).json({
            message: "Delete drink successfully"
        })
    })
}
// Export User controller thành 1 module
module.exports = {
    createUser,
    getAllUser,
    getAllLimitUser,
    getAllSkipUser,
    getAllSortUser,
    getAllSkipLimitUser,
    getAllSortSkipLimitUser,
    getUserById,
    updateUserById,
    deleteUserById,

}