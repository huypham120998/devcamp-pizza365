
//Import thư viện mongooseJs
const mongoose = require("mongoose");

//Khai báo class Schema của thư viện mongooseJs
const Schema = mongoose.Schema;
// khai báo order schema

const orderSchema = new Schema({
    // _id có thể khai bảo hoặc không
    // _id: {
    //     type: mongoose.Types.ObjectId,
    //     required: true
    // },
    orderCode: {
        type: String,
        unique: true
    },
    pizzaSize: {
        type: String,
        required: true
    },
    pizzaType: {
        type: String,
        required: true
    },
    voucher: {
        type: String,
        ref: "Voucher"
    },
    drink: {
        type: String,
        ref: "Drink"
    },
    status: {
        type: String,
        default: "open"
    }

}, {
    timestamps: true
});

module.exports = mongoose.model("Order", orderSchema);
