//Import thư viện mongooseJs
const mongoose = require("mongoose");

//Khai báo class Schema của thư viện mongooseJs
const Schema = mongoose.Schema;
// khai báo drink schema

const drinkSchema = new Schema({
    // _id có thể khai bảo hoặc không
    // _id: {
    //     type: mongoose.Types.ObjectId,
    //     required: true
    // },
    maNuocUong: {
        type: String,
        required: true,
        unique: true
    },
    tenNuocUong: {
        type: String,
        required: true,
    },
    donGia: {
        type: Number,
        required: true,
    },
}, {
    timestamps: true
});

module.exports = mongoose.model("Drink", drinkSchema);

