
//Import thư viện mongooseJs
const mongoose = require("mongoose");

//Khai báo class Schema của thư viện mongooseJs
const Schema = mongoose.Schema;
// khai báo review schema
const voucherSchema = new Schema({
    // _id có thể khai bảo hoặc không
    // _id: {
    //     type: mongoose.Types.ObjectId,
    //     required: true
    // },
    maVoucher: {
        type: String,
        required: true,
        unique: true
    },
    phanTramGiamGia: {
        type: Number,
        required: true
    },
    ghiChu: {
        type: String,
        required: false,
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("Voucher", voucherSchema);
