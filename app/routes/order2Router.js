const express = require("express");
const { createOrder, getDrinkByModel } = require('../controllers/order2Controller');
const order2Router = express.Router();

order2Router.post('/devcamp-pizza365/orders', createOrder);
order2Router.get('/devcamp-pizza365/drinks', getDrinkByModel);

module.exports = order2Router;
