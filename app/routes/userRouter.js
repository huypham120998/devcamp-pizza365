const express = require("express");

const router = express.Router();

const { 
    createUser, 
    getAllUser,
    getAllLimitUser,
    getAllSkipUser,
    getAllSortUser,
    getAllSkipLimitUser,
    getAllSortSkipLimitUser,
    getUserById,
    updateUserById,
    deleteUserById
} = require("../controllers/userController");

router.post("/users", createUser);
router.get("/users", getAllUser);
router.get("/limit-users", getAllLimitUser);
router.get("/skip-users", getAllSkipUser);
router.get("/sort-users", getAllSortUser);
router.get("/skip-limit-users", getAllSkipLimitUser);
router.get("/sort-skip-limit-users", getAllSortSkipLimitUser);
router.get("/users/:userId", getUserById);
router.put("/users/:userId", updateUserById);
router.delete("/users/:userId", deleteUserById);
module.exports = router;
 