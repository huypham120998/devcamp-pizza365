const express = require("express");

const router = express.Router();

const { 
    createDrink, 
    getAllDrink, 
    getDrinkById, 
    updateDrinkById, 
    deleteDrinkById 
} = require("../controllers/drinkController");

router.get("/drinks", getAllDrink);

router.get("/drinks/:drinkId", getDrinkById)

router.post("/drinks", createDrink);

router.put("/drinks/:drinkId", updateDrinkById)

router.delete("/drinks/:drinkId",deleteDrinkById)

module.exports = router;
